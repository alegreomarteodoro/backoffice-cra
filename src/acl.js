import Vue from 'vue'
import store from './store'

export default {

  install(Vue, options) {

    Vue.directive('check-permission', {
      bind (el, binding, vnode, oldVnode) {
        const permission = binding.value;

        if(!Vue.acl._isUserGranted(permission)) {
          //el.style.display = 'none';

          const comment = document.createComment(' ' + permission + ' ');
          Object.defineProperty(comment, 'setAttribute', {
            value: () => undefined,
          });
          vnode.elm = comment;
          vnode.text = ' ';
          vnode.isComment = true;
          vnode.context = undefined;
          vnode.tag = undefined;
          vnode.data.directives = undefined;

          if (vnode.componentInstance) {
            vnode.componentInstance.$el = comment;
          }

          if (el.parentNode) {
            el.parentNode.replaceChild(comment, el);
          }
        }
      }
    })

    Vue.prototype.$acl = Vue.acl = this
  },

  checkPermission (permission) {
    return this._isUserGranted(permission)
  },

  _isUserGranted (permission) {
    const currentUser = store.state.auth.user
    var access = false

    if(currentUser.role && currentUser.role.active) {
      access = currentUser.role.permissions.some(perm => perm.key === permission)
    }

    if (!currentUser.admin && !access) {
      return false
    }

    return true
  }

}
