import Vue from 'vue'

const API_URL = process.env.API_URL;
const GET_USERS_URL = API_URL + 'users';
const GET_ROLES_URL = API_URL + 'roles';
const GET_PERMISSIONS_URL = API_URL + 'permissions';
const GET_CAREERS_URL = API_URL + 'careers';
const GET_STUDENTFILES_URL = API_URL + 'student-files';
const GET_CONFIG_URL = API_URL + 'config';
const GET_REGISTRATION_TO_CAREER_URL = API_URL + 'registration-to-career';
const GET_PAYMENT_URL = API_URL + 'payments';
const GET_PAYMENT_RULE_FOR_FEE_URL = API_URL + 'payment-rules-for-fee';
const GET_PAYMENT_RULE_TO_TUITION_URL = API_URL + 'payment-rules-for-tuition';
const GET_EXAMINATION_TABLES_URL = API_URL + 'examination-tables';
const GET_REGISTRATION_TO_EXAMINATION_TABLES_URL = API_URL + 'registration-to-examination-tables';
const GET_LIST_OF_REGISTRATION_TO_EXAMINATION_TABLES_URL = API_URL + 'list-of-registration-to-examination-tables';
const GET_STOCK_OF_AMBOS_URL = API_URL + 'stock-of-ambos';
const GET_DELIVERY_OF_AMBOS_URL = API_URL + 'delivery-of-ambos';




// Payments

export function getUserPayment(id) {
  return Vue.http.get(GET_PAYMENT_URL + '/' + id)
}

export function getUserPayments() {
  return Vue.http.get(GET_PAYMENT_URL+'/user-payments')
}

export function getAllPayments(params) {
  return Vue.http.get(GET_PAYMENT_URL, { params: params })
}

export function upgradeBaseAmountOnPayments(params) {
  return Vue.http.post(GET_PAYMENT_URL + /upgrade-base-amount/, params)
}

export function createMonthlyFeeOnPayments() {
  return Vue.http.post(GET_PAYMENT_URL + /create-monthly-fee/)
}

export function getFeeInfo(id) {
  return Vue.http.get(GET_PAYMENT_URL + '/pay-fee/' + id)
}

export function getPayment(id) {
  return Vue.http.get(GET_PAYMENT_URL + '/' + id)
}

export function editPayment (payment) {
  return Vue.http.put(GET_PAYMENT_URL, payment)
}

export function deletePayment (id) {
  return Vue.http.delete(GET_PAYMENT_URL + '/' + id)
}

export function getAllPaymentRulesForFee(params) {
  return Vue.http.get(GET_PAYMENT_RULE_FOR_FEE_URL, { params: params })
}

export function getPaymentRuleForFee(id) {
  return Vue.http.get(GET_PAYMENT_RULE_FOR_FEE_URL + '/' + id)
}

export function addPaymentRuleForFee (rule) {
  return Vue.http.post(GET_PAYMENT_RULE_FOR_FEE_URL, rule)
}

export function editPaymentRuleForFee (rule) {
  return Vue.http.put(GET_PAYMENT_RULE_FOR_FEE_URL, rule)
}


export function deletePaymentRuleForFee (id) {
  return Vue.http.delete(GET_PAYMENT_RULE_FOR_FEE_URL + '/' + id)
}


export function getAllPaymentRulesForTuition (params) {
  return Vue.http.get(GET_PAYMENT_RULE_TO_TUITION_URL, { params: params })
}

export function getPaymentRuleForTuition (id) {
  return Vue.http.get(GET_PAYMENT_RULE_TO_TUITION_URL + '/' + id)
}

export function addPaymentRuleForTuition (rule) {
  return Vue.http.post(GET_PAYMENT_RULE_TO_TUITION_URL, rule)
}

export function editPaymentRuleForTuition (rule) {
  return Vue.http.put(GET_PAYMENT_RULE_TO_TUITION_URL, rule)
}

export function deletePaymentRuleForTuition (id) {
  return Vue.http.delete(GET_PAYMENT_RULE_TO_TUITION_URL + '/' + id)
}


// StudentFile

export function addRegistrationForm (form) {
  return Vue.http.post(GET_STUDENTFILES_URL, form)
}

export function editRegistrationForm (form) {
  return Vue.http.put(GET_STUDENTFILES_URL, form)
}

export function getRegistrationForm (user) {
  return Vue.http.post(GET_STUDENTFILES_URL + '/get-by-user', user)
}

export function getAllRegistrationToCareer (params) {
  return Vue.http.get(GET_REGISTRATION_TO_CAREER_URL, { params: params })
}

export function deleteRegistrationToCareer (id) {
  return Vue.http.delete(GET_REGISTRATION_TO_CAREER_URL + '/' + id)
}



// Users

export function getAllUsers (params) {
  return Vue.http.get(GET_USERS_URL, { params: params })
}

export function getUser (id) {
  return Vue.http.get(GET_USERS_URL + '/' + id)
}

export function addUser (user) {
  return Vue.http.post(GET_USERS_URL, user)
}

export function editUser (user) {
  return Vue.http.put(GET_USERS_URL, user)
}

export function deleteUser (id) {
  return Vue.http.delete(GET_USERS_URL + '/' + id)
}

export function verifyAccount (activationCode) {
  return Vue.http.get(GET_USERS_URL + '/veriry-account/' + activationCode)
}



// Roles

export function getAllRoles () {
  return Vue.http.get(GET_ROLES_URL)
}

export function addRole (role) {
  return Vue.http.post(GET_ROLES_URL, role)
}

export function editRole (role) {
  return Vue.http.put(GET_ROLES_URL, role)
}

export function getRole (id) {
  return Vue.http.get(GET_ROLES_URL + '/' + id)
}

export function deleteRole (id) {
  return Vue.http.delete(GET_ROLES_URL + '/' + id)
}



// Permissions

export function getAllPermissions () {
  return Vue.http.get(GET_PERMISSIONS_URL)
}

export function addPermission (permission) {
  return Vue.http.post(GET_PERMISSIONS_URL, permission)
}

export function editPermission (permission) {
  return Vue.http.put(GET_PERMISSIONS_URL, permission)
}

export function getPermission (id) {
  return Vue.http.get(GET_PERMISSIONS_URL + '/' + id)
}

export function deletePermission (id) {
  return Vue.http.delete(GET_PERMISSIONS_URL + '/' + id)
}




// Careers

export function getAllCareers () {
  return Vue.http.get(GET_CAREERS_URL)
}

export function addCareer (permission) {
  return Vue.http.post(GET_CAREERS_URL, permission)
}

export function editCareer (permission) {
  return Vue.http.put(GET_CAREERS_URL, permission)
}

export function getCareer (id) {
  return Vue.http.get(GET_CAREERS_URL + '/' + id)
}

export function deleteCareer (id) {
  return Vue.http.delete(GET_CAREERS_URL + '/' + id)
}




// Config

export function editConfig (config) {
  return Vue.http.put(GET_CONFIG_URL, config)
}

export function getConfig (key) {
  return Vue.http.get(GET_CONFIG_URL + '/' + key)
}

export function getAllConfigurations () {
  return Vue.http.get(GET_CONFIG_URL)
}




// Registration to career - Inscripcion a las carreras

export function editRegistrationToCareer ( registrationToCareer ) {
  return Vue.http.put(GET_REGISTRATION_TO_CAREER_URL, registrationToCareer)
}

export function getRegistrationToCareer ( registrationToCareer ) {
  return Vue.http.post(GET_REGISTRATION_TO_CAREER_URL, registrationToCareer)
}

export function downloadRegistrationToCareerList(params) {
  return Vue.http.post(GET_REGISTRATION_TO_CAREER_URL + '/download', params, {responseType: 'arraybuffer'} )
}





// Examination Tables

export function getAllExaminationTables () {
  return Vue.http.get(GET_EXAMINATION_TABLES_URL)
}

export function addExaminationTable (examinationTable) {
  return Vue.http.post(GET_EXAMINATION_TABLES_URL, examinationTable)
}

export function editExaminationTable (examinationTable) {
  return Vue.http.put(GET_EXAMINATION_TABLES_URL, examinationTable)
}

export function getExaminationTable (id) {
  return Vue.http.get(GET_EXAMINATION_TABLES_URL + '/' + id)
}

export function deleteExaminationTable (id) {
  return Vue.http.delete(GET_EXAMINATION_TABLES_URL + '/' + id)
}




// Registration to Examination Tables

export function getRegistrationToExaminationTables () {
  return Vue.http.get(GET_REGISTRATION_TO_EXAMINATION_TABLES_URL)
}

export function downloadRegistrationToExaminationTablesList(params) {
  return Vue.http.post(GET_REGISTRATION_TO_EXAMINATION_TABLES_URL + '/download', params, {responseType: 'arraybuffer'} )
}

export function addRegistrationToExaminationTables (examinationTable) {
  return Vue.http.post(GET_REGISTRATION_TO_EXAMINATION_TABLES_URL, examinationTable)
}

export function editRegistrationToExaminationTables (examinationTable) {
  return Vue.http.put(GET_REGISTRATION_TO_EXAMINATION_TABLES_URL, examinationTable)
}

export function getListOfRegistrationToExaminationTables ( params ) {
  return Vue.http.get(GET_LIST_OF_REGISTRATION_TO_EXAMINATION_TABLES_URL, { params: params })
}



// Stock of ambos

export function getAllItemsOfStockOfAmbos (params) {
  return Vue.http.get(GET_STOCK_OF_AMBOS_URL, { params: params })
}

export function getItemOfStockOfAmbos (id) {
  return Vue.http.get(GET_STOCK_OF_AMBOS_URL + '/' + id)
}

export function addItemOfStockOfAmbos (item) {
  return Vue.http.post(GET_STOCK_OF_AMBOS_URL, item)
}

export function editItemOfStockOfAmbos (item) {
  return Vue.http.put(GET_STOCK_OF_AMBOS_URL, item)
}

export function deleteItemOfStockOfAmbos (id) {
  return Vue.http.delete(GET_STOCK_OF_AMBOS_URL + '/' + id)
}



// Delivery of ambos

export function getAllItemsOfDeliveryOfAmbos (params) {
  return Vue.http.get(GET_DELIVERY_OF_AMBOS_URL, { params: params })
}

export function getItemOfDeliveryOfAmbos (id) {
  return Vue.http.get(GET_DELIVERY_OF_AMBOS_URL + '/' + id)
}

export function addItemOfDeliveryOfAmbos (item) {
  return Vue.http.post(GET_DELIVERY_OF_AMBOS_URL, item)
}

export function editItemOfDeliveryOfAmbos (item) {
  return Vue.http.put(GET_DELIVERY_OF_AMBOS_URL, item)
}

export function deleteItemOfDeliveryOfAmbos (params) {
  return Vue.http.delete(GET_DELIVERY_OF_AMBOS_URL, { params: params })
}




export function getStudentFileList () {
  return Vue.http.get(GET_STUDENTFILES_URL)
}

export function getListOfStudentsOfCurrentAcademicYear () {
  return Vue.http.get(GET_STUDENTFILES_URL + '/get-list-students-of-current-academic-year')
}
