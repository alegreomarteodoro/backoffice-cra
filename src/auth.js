import Vue from 'vue'
import router from './router'
import store from './store'

const API_URL = process.env.API_URL
const LOGIN_URL = API_URL + 'authenticate'
const SIGNUP_URL = API_URL + 'signup'

export default {

  install (Vue, options) {
    Vue.http.interceptors.push((request, next) => {
      const token = store.state.auth.accessToken
      const hasAuthHeader = request.headers.has('x-access-token')

      if (token && !hasAuthHeader) {
        this.setAuthHeader(request)
      }

      next((response) => {
        if (this._isInvalidToken(response)) {
          return this.logout()
        }
      })
    })

    Vue.prototype.$auth = Vue.auth = this
  },

  login (creds, redirect) {
    const params = { 'email': creds.email, 'password': creds.password }

    return Vue.http.post(LOGIN_URL, params)
      .then((response) => {
        if (this._storeToken(response)) {
          if (redirect) {
            router.push(redirect)
          }
        }

        return response
      })
      .catch((errorResponse) => {
        return errorResponse
      })
  },

  signup (creds, redirect) {
    const params = creds

    return Vue.http.post(SIGNUP_URL, params)
      .then((response) => {
        if (redirect) {
          router.push(redirect)
        }
        return response
      })
      .catch((errorResponse) => {
        return errorResponse
      })
  },

  logout () {
    store.commit('CLEAR_ALL_DATA')
    router.push({ path: '/login' })
  },

  setAuthHeader (request) {
    request.headers.set('x-access-token', store.state.auth.accessToken)
  },

  _retry (request) {
    this.setAuthHeader(request)

    return Vue.http(request)
      .then((response) => {
        return response
      })
      .catch((response) => {
        return response
      })
  },

  _storeToken (response) {
    const auth = store.state.auth

    if (response.body.success) {
      auth.isLoggedIn = true
      auth.accessToken = response.body.access_token
      auth.user.email = response.body.user.email
      auth.user.admin = response.body.user.admin
      auth.user.role = response.body.user.role
      auth.user.displayName = response.body.user.displayName
      auth.user._id = response.body.user._id

      store.commit('UPDATE_AUTH', auth)

      return true
    }

    return false
  },

  _isInvalidToken (response) {
    const status = response.status
    const error = response.data.error

    return (status === 401 && (error === 'invalid_token' || error === 'expired_token'))
  }
}
