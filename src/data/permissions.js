export default [{
  module: 'users',
  key: 'list-users',
  name: 'Listar Usuarios',
  description: 'Poder ver el listado de los usuarios.',
  synded: false
}, {
  module: 'users',
  key: 'edit-users',
  name: 'Editar Usuarios',
  synded: false,
  description: 'Poder agregar, editar y eliminar usuarios.'
}, {
  module: 'roles',
  key: 'list-roles',
  name: 'Listar Roles',
  synded: false,
  description: 'Poder ver el listado de los roles.'
}, {
  module: 'roles',
  key: 'edit-roles',
  name: 'Editar Roles',
  synded: false,
  description: 'Poder agregar, editar y eliminar roles.'
}, {
  module: 'permissions',
  key: 'sync-permissions',
  name: 'Sincronizar Permisos',
  synded: false,
  description: 'Poder sincronizar los permisos de la aplicacion con los del API REST.'
}, {
  module: 'careers',
  key: 'list-careers',
  name: 'Listar Carreras',
  synded: false,
  description: 'Poder ver el listado de carreras.'
}, {
  module: 'careers',
  key: 'edit-careers',
  name: 'Editar Carreras',
  synded: false,
  description: 'Poder agregar, editar y eliminar carreras.'
}, {
  module: 'registration-to-careers',
  key: 'registration-to-careers',
  name: 'Inscripción a carreras',
  synded: false,
  description: 'Completar circuito de inscripción a las carreras.'
}, {
  module: 'config',
  key: 'manage-config',
  name: 'Editar configuraciones del sistema',
  synded: false,
  description: 'Administrar los datos de configuración del sistema.'
}, {
  module: 'config',
  key: 'manage-registration-to-careers',
  name: 'Editar la configuración para la inscripción a carreras',
  synded: false,
  description: 'Administrar los datos de configuración para la inscripción de los alumnos a las carreras.'
}, {
  module: 'registration-to-careers',
  key: 'list-registration-to-careers',
  name: 'Listar inscripciones a las carreras',
  synded: false,
  description: 'Poder ver el listado de inscripciones a las carreras.'
}, {
  module: 'registration-to-careers',
  key: 'approve-registration-to-careers',
  name: 'Aprobar inscripciones a las carreras',
  synded: false,
  description: 'Poder aprobar las inscripciones a las carreras.'
}, {
  module: 'registration-to-careers',
  key: 'cash-out-registration-fee',
  name: 'Cobrar la matricula en efectivo',
  synded: false,
  description: 'Poder cobrar la matricula en efectivo.'
}, {
  module: 'registration-to-careers',
  key: 'delete-registration-to-career',
  name: 'Eliminar inscripciones a las carreras',
  synded: false,
  description: 'Poder eliminar las inscripciones a las carreras.'
}, {
  module: 'payments',
  key: 'list-user-payments',
  name: 'Listar los pagos en el panel de los alumnos',
  synded: false,
  description: 'Poder ver el listado de los pagos en el panel de los alumnos.'
}, {
  module: 'payments',
  key: 'edit-payment-rule',
  name: 'Editar las reglas de pago',
  synded: false,
  description: 'Poder editar las reglas de pago.'
}, {
  module: 'payments',
  key: 'list-payment-rules',
  name: 'Listar las reglas de pago',
  synded: false,
  description: 'Poder ver el listado de las reglas de pago.'
}, {
  module: 'payments',
  key: 'list-payments',
  name: 'Listar todos los pagos para su administración',
  synded: false,
  description: 'Poder ver el listado de los pagos.'
}, {
  module: 'payments',
  key: 'edit-payments',
  name: 'Editar pagos',
  synded: false,
  description: 'Poder editar los pagos.'
}, {
  module: 'users',
  key: 'registration-to-examination-tables',
  name: 'Inscripción a mesas de exámen.',
  synded: false,
  description: 'Poder inscribirse a las mesas de exámen si están inscriptos a una carrera.'
}, {
  module: 'examination-tables',
  key: 'list-registration-to-examination-tables',
  name: 'Listar inscriptos a mesas de exámen.',
  synded: false,
  description: 'Poder ver y exportar el listado de los inscriptos a las mesas de exámen.'
}, {
  module: 'examination-tables',
  key: 'list-examination-tables',
  name: 'Listar las mesas de exámen.',
  synded: false,
  description: 'Poder ver el listado de las mesas de exámen.'
}, {
  module: 'examination-tables',
  key: 'edit-examination-tables',
  name: 'Poder editar las mesas de exámen.',
  synded: false,
  description: 'Poder editar las mesas de exámen.'
}, {
  module: 'sales',
  key: 'list-stock-of-ambos',
  name: 'Listar el stock de los ambos',
  synded: false,
  description: 'Poder ver el listado del stock de los ambos.'
}, {
  module: 'sales',
  key: 'edit-stock-of-ambos',
  name: 'Editar el stock de los ambos',
  synded: false,
  description: 'Poder editar el stock de los ambos.'
}, {
  module: 'sales',
  key: 'list-delivery-of-ambos',
  name: 'Listar la entrega de los ambos',
  synded: false,
  description: 'Poder ver el listado de entrega de los ambos.'
}, {
  module: 'sales',
  key: 'edit-delivery-of-ambos',
  name: 'Editar la entrega de los ambos',
  synded: false,
  description: 'Poder editar la entrega de los ambos.'
}]

