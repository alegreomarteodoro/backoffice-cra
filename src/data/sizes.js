export default [{
  text: 'XS',
  value: 'XS'
}, {
  text: 'S',
  value: 'S'
}, {
  text: 'M',
  value: 'M'
}, {
  text: 'L',
  value: 'L'
}, {
  text: 'XL',
  value: 'XL'
}, {
  text: 'XXL',
  value: 'XXL'
}, {
  text: 'XXXL',
  value: 'XXXL'
}]
