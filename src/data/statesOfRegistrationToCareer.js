export default [{
  text: 'Pendientes (Todos)',
  value: 'pending',
  permit: false
},{
  text: '- Pendiente - Paso 1 (Selección de carrera)',
  value: 'pending-step-1',
  permit: true
},{
  text: '- Pendiente - Paso 2 (Selección de nivel)',
  value: 'pending-step-2',
  permit: true
},{
  text: '- Pendiente - Paso 3 (Actualización de datos)',
  value: 'pending-step-3',
  permit: true
}, {
  text: 'Aprobados (Todos)',
  value: 'approved',
  permit: false
}, {
  text: '- Aprobado Condicional',
  value: 'conditional-approved',
  permit: true
}, {
  text: '- Aprobado Definitivo',
  value: 'final-approved',
  permit: true
}, {
  text: 'Matriculados (Todos)',
  value: 'enrolled',
  permit: false
}, {
  text: '- Matriculado Estándar',
  value: 'enrolled-standard',
  permit: false
}, {
  text: '- Matriculado para Exámenes',
  value: 'enrolled-for-exams',
  permit: false
}, {
  text: '- Matriculado con Beca',
  value: 'enrolled-with-a-scholarship',
  permit: false
}, {
  text: 'Baja (Todos)',
  value: 'suspended',
  permit: false
}, {
  text: '- Baja Definitiva',
  value: 'officially-suspended',
  permit: false
}, {
  text: '- Baja de Oficio',
  value: 'suspended-ex-officio',
  permit: false
}]
