// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import App from './components/App'
import Auth from './auth'
import Acl from './acl'
import Vuelidate from 'vuelidate'
import HtmlCommon from './plugins/html'
import Message from './plugins/message'
import Network from './plugins/network'
import Loader from './plugins/loader'
import UID from './plugins/uid'
import Date from './plugins/date'


const moment = require('moment')
require('moment/locale/es')
Vue.use(require('vue-moment'), { moment })
Vue.use(VueResource)
Vue.use(Auth)
Vue.use(Acl)
Vue.use(Vuelidate)
Vue.use(HtmlCommon)
Vue.use(Message)
Vue.use(Network)
Vue.use(Loader)
Vue.use(UID)
Vue.use(Date)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  created: function () {
    window.Vue = this
    this.$network.checkNetworkConnectivity();
  },
  router,
  store,
  template: '<App/>',
  components: { App }
})
