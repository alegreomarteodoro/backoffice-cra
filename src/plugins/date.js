import Vue from 'vue'

export default {

  install(Vue, options) {
    Vue.prototype.$date = Vue.date = this

    Vue.filter('formatDate', function (date, format) {
      if (!date) return '';

      if(!(date instanceof Date)) {
        date = new Date(date);
      }

      date = date.toISOString().split('T')[0];
      date = date.split('-');

      if(format === 'DD/MM/YYYY') {
        return date[2] + '/' + (date[1]) + '/' + date[0];
      }

      return date[2] + '/' + (date[1]) + '/' + date[0];
    })
  },

  format (date, format) {
    if (!date) return '';

    if(!(date instanceof Date)) {
      date = new Date(date);
    }

    date = date.toISOString().split('T')[0];
    date = date.split('-');

    if(format === 'DD/MM/YYYY') {
      return date[2] + '/' + (date[1]) + '/' + date[0];
    }

    return date[2] + '/' + (date[1]) + '/' + date[0];
  }

}
