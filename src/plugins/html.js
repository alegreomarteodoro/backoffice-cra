import Vue from 'vue'

export default {

  install(Vue, options) {
    Vue.prototype.$html = Vue.html = this
  },

  gotoTop () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  },

  disableButton (key) {
    $(key).attr('disabled', true)
  },

  enableButton (key) {
    $(key).attr('disabled', false)
  }

}
