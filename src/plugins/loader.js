import Vue from 'vue'

export default {

  htmlIdentification: 'body',


  install(Vue, options) {
    Vue.prototype.$loader = Vue.loader = this
  },


  init(htmlIdentification) {
    this.htmlIdentification = htmlIdentification;
  },


  loading(options) {
    var defaults = {
      onStart: function (loading) {
        loading.overlay.slideDown(400);
      },
      onStop: function (loading) {
        loading.overlay.slideUp(400);
      },
      theme: 'dark',
      message: 'Cargando...'
    };

    var settings = $.extend({}, defaults, options);

    $(this.htmlIdentification).loading(settings);
  },


  close() {
    $(this.htmlIdentification).loading('stop');
  }
}
