import Vue from 'vue'

export default {

  install(Vue, options) {
    Vue.prototype.$message = Vue.message = this
  },


  success(options) {
    var defaults = {
      type: 'success',
      title: 'La operacion ha sido exitosa',
      showConfirmButton: false,
      timer: 2000
    }

    var settings = $.extend({}, defaults, options);

    return swal(settings)
  },


  error(options) {
    var defaults = {
      type: 'error',
      title: 'Ha ocurrido un error',
      text: 'Intentalo nuevamente mas tarde.',
      showConfirmButton: false,
      timer: 2000
    }

    var settings = $.extend({}, defaults, options);

    return swal(settings)
  },


  delete(options) {
    var defaults = {
      title: '¿Realmente deseas eliminar el registro?',
      text: 'Una vez eliminado no se podrán recuperar los datos.',
      type: "warning",
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonColor: '#eb7475',
      cancelButtonColor: '#eceeef',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
      width: 700
    }

    var settings = $.extend({}, defaults, options);

    return swal(settings);
  },



  confirm(options) {
    var defaults = {
      title: '¿Realmente deseas realizar esta operación?',
      text: 'Una vez confirmado no podras deshacer la acción.',
      type: "warning",
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonColor: '#eb7475',
      cancelButtonColor: '#eceeef',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
      width: 700
    }

    var settings = $.extend({}, defaults, options);

    return swal(settings);
  },


  showLoading() {
    return swal({
      title: 'Cargando...',
      text: 'Aguarde un momento por favor...',
      showConfirmButton: false,
      onOpen: () => {
        swal.showLoading()
      }
    })
  },


  closeLoading() {
    swal.close();
  }

}
