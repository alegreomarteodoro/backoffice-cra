import Vue from 'vue'

export default {

  install(Vue, options) {
    Vue.prototype.$uid = Vue.uid = this
  },

  generateUID() {
    var uuid = "", i, random;
    for (i = 0; i < 24; i++) {
      random = Math.random() * 16 | 0;

      uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
    }
    return uuid;
  }
}
