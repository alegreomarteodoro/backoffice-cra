import Vue from 'vue'
import Router from 'vue-router'
import HomeScreen from '@/screens/HomeScreen'
import LoginScreen from '@/screens/LoginScreen'
import SignupScreen from '@/screens/SignupScreen'
import VerifyAccountScreen from '@/screens/VerifyAccountScreen'
import UserListScreen from '@/screens/UserListScreen'
import RoleListScreen from '@/screens/RoleListScreen'
import EditRoleScreen from '@/screens/EditRoleScreen'
import EditUserScreen from '@/screens/EditUserScreen'
import PermissionManagerScreen from '@/screens/PermissionManagerScreen'
import RestrictedContentScreen from '@/screens/RestrictedContentScreen'
import CareerListScreen from '@/screens/CareerListScreen'
import EditCareerScreen from '@/screens/EditCareerScreen'
import NotFoundScreen from '@/screens/NotFoundScreen'
import StudentRegistrationStep1 from '@/screens/student_registration/StudentRegistrationStep1'
import StudentRegistrationStep2 from '@/screens/student_registration/StudentRegistrationStep2'
import StudentRegistrationStep3 from '@/screens/student_registration/StudentRegistrationStep3'
import PrintStudentRegistration from '@/screens/student_registration/PrintStudentRegistration'
import ConfigScreen from '@/screens/ConfigScreen'
import RegistrationFeeScreenInCash from '@/screens/RegistrationFeeScreenInCash'
import RegistrationListToCareersScreen from '@/screens/RegistrationListToCareersScreen'
import ApproveRegistrationToCareerScreen from '@/screens/ApproveRegistrationToCareerScreen'
import UserPaymentListScreen from '@/screens/UserPaymentList'
import PayFeeScreen from '@/screens/PayFeeScreen'
import PayTuitionScreen from '@/screens/PayTuitionScreen'
import PaymentRulesForFeeScreen from '@/screens/PaymentRulesForFeeScreen'
import EditPaymentRuleForFeeScreen from '@/screens/EditPaymentRuleForFeeScreen'
import PaymentListScreen from '@/screens/PaymentListScreen'
import EditPaymentScreen from '@/screens/EditPaymentScreen'
import ExaminationTablesListScreen from '@/screens/ExaminationTablesListScreen'
import EditExaminationTablesScreen from '@/screens/EditExaminationTablesScreen'
import RegistrationToExaminationTablesScreen from '@/screens/RegistrationToExaminationTablesScreen'
import RegistrationToExaminationTablesListScreen from '@/screens/RegistrationToExaminationTablesListScreen'
import RegistrationToCareersScreen from '@/screens/WelcomeRegistrationToCareers'
import PaymentRulesForTuitionScreen from '@/screens/PaymentRulesForTuitionScreen'
import EditPaymentRuleForTuitionScreen from '@/screens/EditPaymentRuleForTuitionScreen'
import StockOfAmbosListScreen from '@/screens/StockOfAmbosListScreen'
import EditItemOfStockOfAmbosScreen from '@/screens/EditItemOfStockOfAmbosScreen'
import DeliveryOfAmbosListScreen from '@/screens/DeliveryOfAmbosListScreen'
import EditItemOfDeliveryOfAmbosScreen from '@/screens/EditItemOfDeliveryOfAmbosScreen'
import EditIncriptionIntoRegistrationToExaminationTables from '@/screens/EditIncriptionIntoRegistrationToExaminationTables'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeScreen,
      beforeEnter: guardRoute
    },

    {
      path: '/restricted',
      name: 'restricted-content',
      component: RestrictedContentScreen
    },

    {
      path: '/login',
      name: 'login',
      component: LoginScreen,
      beforeEnter: checkIsLoggedIn
    },

    {
      path: '/signup',
      name: 'signup',
      component: SignupScreen,
      beforeEnter: checkIsLoggedIn
    },

    {
      path: '/verify-account/:activationCode?',
      name: 'verify-account',
      component: VerifyAccountScreen,
      beforeEnter: checkIsLoggedIn
    },

    {
      path: '/users',
      name: 'user-list',
      component: UserListScreen,
      meta: {
        permission: 'list-users'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-user/:id?',
      name: 'edit-user',
      component: EditUserScreen,
      meta: {
        permission: 'edit-users'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/roles',
      name: 'role-list',
      component: RoleListScreen,
      meta: {
        permission: 'list-roles'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/permission-manager',
      name: 'permission-manager',
      component: PermissionManagerScreen,
      meta: {
        permission: 'sync-permissions'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-role/:id?',
      name: 'edit-role',
      component: EditRoleScreen,
      meta: {
        permission: 'edit-roles'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/careers',
      name: 'career-list',
      component: CareerListScreen,
      meta: {
        permission: 'list-careers'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-career/:id?',
      name: 'edit-career',
      component: EditCareerScreen,
      meta: {
        permission: 'edit-careers'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/student-registration-step-1',
      name: 'student-registration-step-1',
      component: StudentRegistrationStep1,
      beforeEnter: guardRoute
    },

    {
      path: '/student-registration-step-2',
      name: 'student-registration-step-2',
      component: StudentRegistrationStep2,
      beforeEnter: guardRoute
    },

    {
      path: '/student-registration-step-3/',
      name: 'student-registration-step-3',
      component: StudentRegistrationStep3,
      beforeEnter: guardRoute
    },

    {
      path: '/print-student-registration/',
      name: 'PrintForm',
      component: PrintStudentRegistration,
      beforeEnter: guardRoute
    },

    {
      path: '/config',
      name: 'config',
      component: ConfigScreen,
      meta: {
        permission: 'manage-config'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/registration-to-careers',
      name: 'registration-to-careers',
      component: RegistrationToCareersScreen,
      meta: {
        permission: 'registration-to-careers'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-registration-to-careers',
      name: 'list-registration-to-careers',
      component: RegistrationListToCareersScreen,
      meta: {
        permission: 'list-registration-to-careers'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-user-payments',
      name: 'list-user-payments',
      component: UserPaymentListScreen,
      meta: {
        permission: 'list-user-payments'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-payments',
      name: 'list-payments',
      component: PaymentListScreen,
      meta: {
        permission: 'list-payments'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/pay-fee/:id?',
      name: 'pay-fee',
      component: PayFeeScreen,
      meta: {
        permission: 'list-user-payments'
      },
      beforeEnter: guardRoute
    },


    {
      path: '/pay-tuition/:id?',
      name: 'pay-tuition',
      component: PayTuitionScreen,
      meta: {
        permission: 'list-user-payments'
      },
      beforeEnter: guardRoute
    },


    {
      path: '/edit-payment/:id?',
      name: 'edit-payment',
      component: EditPaymentScreen,
      meta: {
        permission: 'edit-payments'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/payment-rules-for-fee',
      name: 'payment-rules-for-fee',
      component: PaymentRulesForFeeScreen,
      meta: {
        permission: 'list-payment-rules'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-payment-rule-for-fee/:id?',
      name: 'edit-payment-rule-for-fee',
      component: EditPaymentRuleForFeeScreen,
      meta: {
        permission: 'edit-payment-rule'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/payment-rules-for-tuition',
      name: 'payment-rules-for-tuition',
      component: PaymentRulesForTuitionScreen,
      meta: {
        permission: 'list-payment-rules'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-payment-rule-for-tuition/:id?',
      name: 'edit-payment-rule-for-tuition',
      component: EditPaymentRuleForTuitionScreen,
      meta: {
        permission: 'edit-payment-rule'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/approve-registration-to-careers/:id?',
      name: 'approve-registration-to-careers',
      component: ApproveRegistrationToCareerScreen,
      meta: {
        permission: 'approve-registration-to-careers'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/cash-out-registration-fee/:id?',
      name: 'cash-out-registration-fee',
      component: RegistrationFeeScreenInCash,
      meta: {
        permission: 'cash-out-registration-fee'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/examination-tables',
      name: 'examination-tables-list',
      component: ExaminationTablesListScreen,
      meta: {
        permission: 'list-examination-tables'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-examination-tables/:id?',
      name: 'edit-examination-tables',
      component: EditExaminationTablesScreen,
      meta: {
        permission: 'edit-examination-tables'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/registration-to-examination-tables',
      name: 'registration-to-examination-tables',
      component: RegistrationToExaminationTablesScreen,
      meta: {
        permission: 'registration-to-examination-tables'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-of-registration-to-examination-tables',
      name: 'list-registration-to-examination-tables',
      component: RegistrationToExaminationTablesListScreen,
      meta: {
        permission: 'list-registration-to-examination-tables'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-stock-of-ambos',
      name: 'list-stock-of-ambos',
      component: StockOfAmbosListScreen,
      meta: {
        permission: 'list-stock-of-ambos'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-stock-of-ambos',
      name: 'edit-stock-of-ambos',
      component: EditItemOfStockOfAmbosScreen,
      meta: {
        permission: 'edit-stock-of-ambos'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/list-delivery-of-ambos',
      name: 'list-delivery-of-ambos',
      component: DeliveryOfAmbosListScreen,
      meta: {
        permission: 'list-delivery-of-ambos'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-delivery-of-ambos',
      name: 'edit-delivery-of-ambos',
      component: EditItemOfDeliveryOfAmbosScreen,
      meta: {
        permission: 'edit-delivery-of-ambos'
      },
      beforeEnter: guardRoute
    },

    {
      path: '/edit-inscripcion-into-registration-to-examination-tables',
      name: 'edit-inscripcion-into-registration-to-examination-tables',
      component: EditIncriptionIntoRegistrationToExaminationTables,
      meta: {
        permission: 'list-registration-to-examination-tables'
      },
      beforeEnter: guardRoute
    },

    {
      path: "*",
      component: NotFoundScreen
    }
  ]
})

function guardRoute (to, from, next) {
  const auth = router.app.$options.store.state.auth
  const currentUser = router.app.$options.store.state.auth.user
  const routeRecord = to.matched.find(record => record.meta.permission)

  if (!auth.isLoggedIn) {
    next({
      path: '/login'
    })
  } else if(auth.isLoggedIn && routeRecord) {
    const permission = routeRecord.meta.permission

    if(currentUser.admin) next()

    if(currentUser.role && currentUser.role.permissions) {
      if(currentUser.role.permissions.some(perm => perm.key == permission)) {
        next()
      } else {
        next({
          path: '/restricted'
        })
      }
    }

  } else {
    next()
  }
}

function checkIsLoggedIn (to, from, next) {
  const auth = router.app.$options.store.state.auth

  if (auth.isLoggedIn) {
    next({
      path: '/'
    })
  } else {
    next()
  }
}

export default router
