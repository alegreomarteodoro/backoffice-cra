import * as api from '../api'


// mobile menu
export const showMobileMenu = function ({ commit }, { show }) {
  commit('SHOW_MOBILE_MENU', show)
}

// users

export const getAllUsers = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllUsers(params)
      .then((response) => {
        if (response.body.success) {
          commit('GET_USERS', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const getUser = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getUser(id)
        .then((response) => {
        if (response.body.success) {
          commit('UPDATE_USER', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
    })
    .catch(error => {
        reject(error)
      })
    } else {
      let user = {
        _id: null,
        email: null,
        admin: false,
        active: false,
        role: null,
        password: null,
        displayName: null
      }
      commit('UPDATE_USER', user)
  resolve(user)
}
})
}

export const setUser = function ({ commit }, { user }) {
  commit('UPDATE_USER', user)
}

export const deleteUser = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteUser(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_USER', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editUser = function ({ commit }, { user }) {
  return new Promise((resolve, reject) => {
    if (!user._id) {
    api.addUser(user)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_USER', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  } else {
    api.editUser(user)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_USER', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  }
})
}

// roles

export const getAllRoles = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getAllRoles()
    .then((response) => {
      if (response.body.success) {
        commit('GET_ROLES', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
      reject(error)
    })
  })
}

export const editRole = function ({ commit }, { role }) {
  return new Promise((resolve, reject) => {
    if (!role._id) {
      //console.log(role)
      api.addRole(role)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_ROLE', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
          reject(error)
        })
    } else {
      api.editRole(role)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_ROLE', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
          reject(error)
        })
    }
  })
}

export const getRole = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getRole(id)
        .then((response) => {
        if (response.body.success) {
          commit('UPDATE_ROLE', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
          reject(error)
      })
    } else {
      let role = {
        name: '',
        _id: null,
        active: false,
        permissions: []
      }
      commit('UPDATE_ROLE', role)
      resolve(role)
    }
  })
}

export const setRole = function ({ commit }, { role }) {
  commit('UPDATE_ROLE', role)
}

export const deleteRole = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteRole(id)
      .then((response) => {
        if (response.body.success) {
          commit('DELETE_ROLE', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
          reject(error)
      })
  })
}


// Permissions

export const getAllPermissions = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getAllPermissions()
    .then((response) => {
    if (response.body.success) {
    commit('GET_PERMISSIONS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const addPermission = function ({ commit }, { permission }) {
  return new Promise((resolve, reject) => {
    api.addPermission(permission)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_PERMISSION', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
})
}

export const getPermission = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getPermission(id)
        .then((response) => {
        if (response.body.success) {
        commit('UPDATE_PERMISSION', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
    } else {
      let permission = {
        _id: null,
        name: '',
        key: '',
        module: ''
      }
      commit('UPDATE_PERMISSION', permission)
    resolve(permission)
}
})
}

export const setPermission = function ({ commit }, { permission }) {
  commit('UPDATE_PERMISSION', permission)
}

export const deletePermission = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deletePermission(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_PERMISSION', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}



// Careers

export const getAllCareers = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getAllCareers()
    .then((response) => {
    if (response.body.success) {
    commit('GET_CAREERS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editCareer = function ({ commit }, { career }) {
  return new Promise((resolve, reject) => {
    if (!career._id) {
    api.addCareer(career)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_CAREER', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  } else {
    api.editCareer(career)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_CAREER', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  }
})
}

export const getCareer = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getCareer(id)
        .then((response) => {
        if (response.body.success) {
        commit('UPDATE_CAREER', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
    } else {
      let career = {
        _id: null,
        name: '',
        active: false,
        studyPlan: null
      }
      commit('UPDATE_CAREER', career)
      resolve(career)
}
})
}

export const setCareer = function ({ commit }, { career }) {
  commit('UPDATE_CAREER', career)
}

export const deleteCareer = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteCareer(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_CAREER', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}


//form
export const cleanRegistrationForm = function ({ commit }) {
  commit('CLEAR_FORM')
}

export const addRegistrationForm = function ({ commit },{ form }) {
  return new Promise((resolve, reject) => {
    api.addRegistrationForm({studentFile:form})
    .then((response) => {
    if (response.body.success) {
    commit('UPDATE_FORM', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editRegistrationForm = function ({ commit },{ form }) {
  return new Promise((resolve, reject) => {
    api.editRegistrationForm({ studentFile: form })
    .then((response) => {
      if (response.body.success) {
        commit('UPDATE_FORM', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
      reject(error)
    })
  })
}

export const getRegistrationForm = function ({ commit },{ user }) {
  return new Promise((resolve, reject) => {
    api.getRegistrationForm( { user : user } )
    .then((response) => {
    if (response.body.success) {
      if(response.body.data) {
        commit('UPDATE_FORM', response.body.data)
      } else {
        commit('CLEAR_FORM')
      }
      resolve(response.body)
  } else {
      reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const getAllConfigurations = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getAllConfigurations()
      .then((response) => {
        if (response.body.success) {
          commit('GET_CONFIGURATIONS', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const editConfig = function ({ commit },{ config }) {
  return new Promise((resolve, reject) => {
    api.editConfig( config )
    .then((response) => {
      if (response.body.success) {
        commit('UPDATE_CONFIG', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
  })
}


export const getConfigByKey = function ({ commit },{ key }) {
  return new Promise((resolve, reject) => {
    api.editConfig( config )
    .then((response) => {
    if (response.body.success) {
    commit('UPDATE_CONFIG', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}


// registration to career

export const getRegistrationToCareer = function ({ commit }, { registrationToCareer }) {
  return new Promise((resolve, reject) => {
    api.getRegistrationToCareer(registrationToCareer)
      .then((response) => {
        if (response.body.success) {
          commit('GET_REGISTRATION_TO_CAREER', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}


export const editRegistrationToCareer = function ({ commit },{ registrationToCareer }) {
  return new Promise((resolve, reject) => {
    api.editRegistrationToCareer( registrationToCareer )
      .then((response) => {
        if (response.body.success) {
          commit('UPDATE_REGISTRATION_TO_CAREER', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
})
}

export const getAllRegistrationToCareer = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllRegistrationToCareer(params)
    .then((response) => {
      if (response.body.success) {
        commit('GET_REGISTRATION_TO_CAREER_LIST', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
    })
  })
}

export const setRegistrationToCareer = function ({ commit }, { registrationToCareer }) {
  commit('UPDATE_REGISTRATION_TO_CAREER', registrationToCareer)
}

export const deleteRegistrationToCareer = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteRegistrationToCareer(id)
    .then((response) => {
      if (response.body.success) {
        commit('DELETE_REGISTRATION_TO_CAREER', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
    })
}

// Payments
export const getUserPayment = function ({ commit },{id}) {
  return new Promise((resolve, reject) => {
    api.getUserPayment(id)
      .then((response) => {
        if (response.body.success) {
          commit('GET_USER_PAYMENT', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const getUserPayments = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getUserPayments()
      .then((response) => {
        if (response.body.success) {
          commit('GET_USER_PAYMENTS', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}


export const getAllPayments = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllPayments(params)
    .then((response) => {
    if (response.body.success) {
    commit('GET_PAYMENTS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const getPayment = function ({ commit },{id}) {
  return new Promise((resolve, reject) => {
    api.getPayment(id)
    .then((response) => {
    if (response.body.success) {
    commit('GET_PAYMENT', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const deletePayment = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deletePayment(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_PAYMENT', response.body.data)
    resolve(response.body);
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
};


// Payments
export const getPaymentRuleForFee = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {

      api.getPaymentRuleForFee(id)
      .then((response) => {
        if (response.body.success) {
          commit('GET_PAYMENT_RULE_FOR_FEE', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
          reject(error)
      })

    } else {

      let paymentRule = {
        career: null,
        name: '',
        active : false,
        baseAmount: null,
        discount: {
          untilDay: null,
          amount: null
        },
        expirationDay: null,
        interests: []
      }
      commit('GET_PAYMENT_RULE_FOR_FEE', paymentRule)
      resolve(paymentRule)

    }
  })
}



export const editPaymentRuleForFee = function ({ commit }, { rule }) {
  return new Promise((resolve, reject) => {
    if (!rule._id) {
      api.addPaymentRuleForFee(rule)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_PAYMENT_RULE_FOR_FEE', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
            reject(error)
          })
    } else {
      api.editPaymentRuleForFee(rule)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_PAYMENT_RULE_FOR_FEE', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
            reject(error)
          })
    }
  })
}



export const getAllPaymentRulesForFee = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllPaymentRulesForFee(params)
    .then((response) => {
    if (response.body.success) {
    commit('GET_PAYMENT_RULES_FOR_FEE', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const deletePaymentRuleForFee = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deletePaymentRuleForFee(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_PAYMENT_RULE_FOR_FEE', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}



export const editPayment = function ({ commit }, { payment }) {
  return new Promise((resolve, reject) => {
    api.editPayment(payment)
      .then((response) => {
        if (response.body.success) {
          commit('UPDATE_PAYMENT', response.body.data)
          resolve(response.body)
        } else {
          reject(response.body)
        }
      })
      .catch(error => {
          reject(error)
      })
  })
}





// Examination Tables

export const getAllExaminationTables = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getAllExaminationTables()
    .then((response) => {
    if (response.body.success) {
    commit('GET_EXAMINATION_TABLES', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editExaminationTable = function ({ commit }, { examinationTable }) {
  return new Promise((resolve, reject) => {
    if (!examinationTable._id) {
    api.addExaminationTable(examinationTable)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_EXAMINATION_TABLE', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  } else {
    api.editExaminationTable(examinationTable)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_EXAMINATION_TABLE', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  }
})
}

export const getExaminationTable = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getExaminationTable(id)
        .then((response) => {
        if (response.body.success) {
        commit('UPDATE_EXAMINATION_TABLE', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
    } else {
      let examinationTable = {
        _id: null,
        name: '',
        description: '',
        dateFrom : null,
        dateTo : null,
        active : false,
        careers: []
      };
      commit('UPDATE_EXAMINATION_TABLE', examinationTable)
  resolve(examinationTable)
}
})
}

export const setExaminationTable = function ({ commit }, { examinationTable }) {
  commit('UPDATE_EXAMINATION_TABLE', examinationTable)
}

export const deleteExaminationTable = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteExaminationTable(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_EXAMINATION_TABLE', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}





// Registration to Examination Tables

export const getRegistrationToExaminationTables = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getRegistrationToExaminationTables()
    .then((response) => {
    if (response.body.success) {
    commit('GET_REGISTRATION_TO_EXAMINATION_TABLES', response.body.data)
    resolve(response.body)
  } else {
    commit('GET_REGISTRATION_TO_EXAMINATION_TABLES', {})
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const getListOfRegistrationToExaminationTables = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getListOfRegistrationToExaminationTables(params)
    .then((response) => {
      if (response.body.success) {
        commit('GET_LIST_OF_REGISTRATION_TO_EXAMINATION_TABLES', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error);
      })
  })
};




export const getPaymentRuleForTuition = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {

      api.getPaymentRuleForTuition(id)
        .then((response) => {
        if (response.body.success) {
        commit('GET_PAYMENT_RULE_FOR_TUITION', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })

    } else {

      let paymentRule = {
        career: null,
        name: '',
        active : false,
        baseAmount: null,
        discount: {
          untilDay: null,
          amount: null
        },
        expirationDay: null,
        interests: []
      }
      commit('GET_PAYMENT_RULE_FOR_TUITION', paymentRule)
  resolve(paymentRule)

}
})
}



export const editPaymentRuleForTuition = function ({ commit }, { rule }) {
  return new Promise((resolve, reject) => {
    if (!rule._id) {
    api.addPaymentRuleForTuition(rule)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_PAYMENT_RULE_FOR_TUITION', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  } else {
    api.editPaymentRuleForTuition(rule)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_PAYMENT_RULE_FOR_TUITION', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  }
})
}



export const getAllPaymentRulesForTuition = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllPaymentRulesForTuition(params)
    .then((response) => {
    if (response.body.success) {
    commit('GET_PAYMENT_RULES_FOR_TUITION', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const deletePaymentRuleForTuition = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deletePaymentRuleForTuition(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_PAYMENT_RULE_FOR_TUITION', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}





// Stock of ambos

export const getAllItemsOfStockOfAmbos = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllItemsOfStockOfAmbos(params)
    .then((response) => {
    if (response.body.success) {
    commit('GET_STOCK_OF_AMBOS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editItemOfStockOfAmbos = function ({ commit }, { item }) {
  return new Promise((resolve, reject) => {
    if (!item._id) {
      api.addItemOfStockOfAmbos(item)
        .then((response) => {
            if (response.body.success) {
              commit('UPDATE_STOCK_OF_AMBOS', response.body.data)
              resolve(response.body)
            } else {
              reject(response.body)
            }
        })
        .catch(error => {
            reject(error)
        })
    } else {
      api.editItemOfStockOfAmbos(item)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_STOCK_OF_AMBOS', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
            reject(error)
          })
        }
  })
};

export const getItemOfStockOfAmbos = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getItemOfStockOfAmbos(id)
        .then((response) => {
          if (response.body.success) {
            commit('UPDATE_STOCK_OF_AMBOS', response.body.data)
            resolve(response.body)
          } else {
            reject(response.body)
          }
        })
        .catch(error => {
            reject(error)
          })
    } else {
      let item = {
        _id: null,
        name: null,
        description: null,
        career: null,
        size: null,
        amount: 0,
        price: 0
      }
      commit('UPDATE_STOCK_OF_AMBOS', item)
      resolve(item)
    }
  })
};

export const deleteItemOfStockOfAmbos = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    api.deleteItemOfStockOfAmbos(id)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_STOCK_OF_AMBOS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}






// Delivery of ambos

export const getAllItemsOfDeliveryOfAmbos = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.getAllItemsOfDeliveryOfAmbos(params)
    .then((response) => {
    if (response.body.success) {
    commit('GET_DELIVERY_OF_AMBOS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const editItemOfDeliveryOfAmbos = function ({ commit }, { item }) {
  return new Promise((resolve, reject) => {
    if (!item._id) {
    api.addItemOfDeliveryOfAmbos(item)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_DELIVERY_OF_AMBOS', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  } else {
    api.editItemOfDeliveryOfAmbos(item)
      .then((response) => {
      if (response.body.success) {
      commit('UPDATE_DELIVERY_OF_AMBOS', response.body.data)
      resolve(response.body)
    } else {
      reject(response.body)
    }
  })
  .catch(error => {
      reject(error)
    })
  }
})
};

export const getItemOfDeliveryOfAmbos = function ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    if (id) {
      api.getItemOfDeliveryOfAmbos(id)
        .then((response) => {
        if (response.body.success) {
        commit('UPDATE_DELIVERY_OF_AMBOS', response.body.data)
        resolve(response.body)
      } else {
        reject(response.body)
      }
    })
    .catch(error => {
        reject(error)
      })
    } else {
      let item = {
        _id: null,
        status: null,
        comments: null,
        deliveredProduct: null,
        deliveredQuantity: 1,
        deliveredToUser: null
      }
      commit('UPDATE_DELIVERY_OF_AMBOS', item)
  resolve(item)
}
})
};

export const deleteItemOfDeliveryOfAmbos = function ({ commit }, { params }) {
  return new Promise((resolve, reject) => {
    api.deleteItemOfDeliveryOfAmbos(params)
    .then((response) => {
    if (response.body.success) {
    commit('DELETE_DELIVERY_OF_AMBOS', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}



// Student file list
export const getStudentFileList = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getStudentFileList()
    .then((response) => {
    if (response.body.success) {
    commit('GET_STUDENT_FILE_LIST', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}


export const getListOfStudentsOfCurrentAcademicYear = function ({ commit }) {
  return new Promise((resolve, reject) => {
    api.getListOfStudentsOfCurrentAcademicYear()
    .then((response) => {
    if (response.body.success) {
    commit('GET_STUDENT_LIST_OF_CURRENT_ACADEMIC_YEAR', response.body.data)
    resolve(response.body)
  } else {
    reject(response.body)
  }
})
.catch(error => {
    reject(error)
  })
})
}

export const cleanPayments = function ({ commit }) {
  commit('CLEAN_PAYMENTS')
}
