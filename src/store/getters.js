// Mobile menu
export const getMobileMenuStatus = state => state.mobileMenuIsActive


// Users
export const getRegistrationToCareerList = state => state.registrationToCareerList

export const getRegistrationForm = state => state.registrationForm

export const getUser = state => state.user

export const getAllUsers = state => state.users

export const getAdminUsers = function (state) {
  return state.users.docs.filter(user => user.admin)
}


// Roles
export const getAllRoles = state => state.roles

export const getRole = state => state.role


// Permissions
export const getAllPermissions = state => state.permissions

export const getPermission = state => state.permission


// Careers
export const getAllCareers = state => state.careers

export const getCareer = state => state.career


// Configs
export const getAllConfigurations = state => state.configurations

export const getConfigByKey = state => (key) => {
  const results = state.configurations.filter(configItem => configItem.key === key)
  if(results.length > 0)
    return results[0]
}


// Registration to career
export const getRegistrationToCareer = state => state.registrationToCareer


// Payments
export const getUserPayments = state => state.payments

export const getAllPayments = state => state.payments

export const getPayment = state => state.payment

export const getUserPayment = state => state.payment

export const getPaymentRule = state => state.paymentRule

export const getAllPaymentRulesForFee = state => state.paymentRulesForFee

export const getAllPaymentRulesForTuition = state => state.paymentRulesForTuition


// Examination Tables
export const getAllExaminationTables = state => state.examinationTables

export const getExaminationTable = state => state.examinationTable



// Examination Tables
export const getRegistrationToExaminationTables = state => state.registrationToExaminationTables

export const getListOfRegistrationToExaminationTables = state => state.listOfRegistrationToExaminationTables



// Stock of ambos
export const getAllItemsOfStockOfAmbos = state => state.itemListOfStockOfAmbos

export const getItemOfStockOfAmbos = state => state.itemOfStockOfAmbos



// Delivery of ambos
export const getAllItemsOfDeliveryOfAmbos = state => state.itemListOfDeliveryOfAmbos

export const getItemOfDeliveryOfAmbos = state => state.itemOfDeliveryOfAmbos


// Student file list
export const getStudentFileList = state => state.studentFileList


export const getListOfStudentsOfCurrentAcademicYear = state => state.studentsListOfCurrentAcademicYear
