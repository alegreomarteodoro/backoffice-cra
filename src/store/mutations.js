export const UPDATE_AUTH = (state, auth) => {
  state.auth = auth
}

export const CLEAR_ALL_DATA = (state) => {
  // Auth
  state.auth.isLoggedIn = false
  state.auth.accessToken = null

  // User
  state.user.email = null
  state.user.admin = false
  state.user.role = null

  // Users
  state.users = []

  // Roles
  state.roles = []
  state.role = null

  // Permissions
  state.permissions = []
  state.permission = null

  // Careers
  state.careers = []

}

// Form

export const GET_REGISTRATION_TO_CAREER_LIST = (state, items) => {
  state.registrationToCareerList = items
}
export const UPDATE_FORM = (state, registrationForm) => {
  state.registrationForm = registrationForm
}
export const CLEAR_FORM = (state) => {
  state.registrationForm =
    {
      _id:null,
      currentCareer:null,
      currentLevel: null,
      personalData:{
        lastName:null,
        firstName:null,
        dni:null,
        gender:null,
        cuil:null,
        size:null,
        birthDate:null,
        email:null,
        birthPlace: {
          locality:null,
          province:null,
          nationality:null
        },
        currentResidence: {
          street:null,
          streetNumber:null,
          floor:null,
          departamentNumber:null,
          neighborhood:null,
          locality:null,
          province:null,
          country: null,
          zipCode:null,
          phone:null,
          cellPhone:null,
          cellPhoneCompany:null,
        }
      },
      treasury: {
        paymentId: null,
        paymentMethod: null
      }
    }
}



// Mobile menu

export const SHOW_MOBILE_MENU = (state, isActive) => {
  state.mobileMenuIsActive = isActive
}


// Users

export const GET_USERS = (state, users) => {
  state.users = users
}

export const DELETE_USER = (state, user) => {
  state.users.docs = state.users.docs.filter(function (item) {
    var result = user._id != item._id;
    if(!result) {
      state.users.total--;
    }
    return result;
  })
}

export const UPDATE_USER = (state, user) => {
  state.user = user
}


// Roles

export const GET_ROLES = (state, roles) => {
  state.roles = roles
}

export const UPDATE_ROLE = (state, role) => {
  state.role = role
}

export const DELETE_ROLE = (state, role) => {
  state.roles = state.roles.filter(function (item) {
    return role._id != item._id;
  })
}


// Permissions

export const GET_PERMISSIONS = (state, permissions) => {
  state.permissions = permissions
}

export const UPDATE_PERMISSION = (state, permission) => {
  state.permission = permission
}

export const DELETE_PERMISSION = (state, permission) => {
  state.permissions = state.permissions.filter(function (item) {
    return permission._id != item._id;
  })
}


// Carrers

export const GET_CAREERS = (state, careers) => {
  state.careers = careers
}

export const UPDATE_CAREER = (state, career) => {
  state.career = career
}

export const DELETE_CAREER = (state, career) => {
  state.careers = state.careers.filter(function (item) {
    return career._id != item._id;
  })
}

// Configurations

export const GET_CONFIGURATIONS = (state, configurations) => {
  state.configurations = configurations
}

export const UPDATE_CONFIG = (state, config) => {
  state.configurations.filter(function (item) {
    if (item.key === config.key) {
      item.value = config.value
    }
  })
}

// Registration to career

export const GET_REGISTRATION_TO_CAREER = (state, registrationToCareer) => {
  state.registrationToCareer = registrationToCareer
}

export const UPDATE_REGISTRATION_TO_CAREER = (state, registrationToCareer) => {
  state.registrationToCareer = registrationToCareer
}

export const DELETE_REGISTRATION_TO_CAREER = (state, registrationToCareer) => {
  state.registrationToCareerList.docs = state.registrationToCareerList.docs.filter(function (item) {
    var result = registrationToCareer._id != item._id;
    if(!result) {
      state.registrationToCareerList.total--;
    }
    return result;
  })
}

// Payments

export const GET_USER_PAYMENTS = (state, payments) => {
  state.payments = payments
}

export const GET_PAYMENTS = (state, payments) => {
  state.payments = payments
}

export const CLEAN_PAYMENTS = (state) => {
  state.payments = {
    docs : []
  }
}

export const GET_USER_PAYMENT = (state, payment) => {
  state.payment = payment
}

export const GET_PAYMENT = (state, payment) => {
  state.payment = payment
}

export const DELETE_PAYMENT = (state, payment) => {
  state.payments.docs = state.payments.docs.filter(function (item) {
    var result = payment._id != item._id;
    if(!result) {
      state.payments.total--;
    }
    return result;
  })
}

export const UPDATE_PAYMENT = (state, payment) => {
  state.payment = payment
}

export const GET_PAYMENT_RULE_FOR_FEE = (state, paymentRule) => {
  state.paymentRule = paymentRule
}

export const UPDATE_PAYMENT_RULE_FOR_FEE = (state, paymentRule) => {
  state.paymentRule = paymentRule
}

export const GET_PAYMENT_RULES_FOR_FEE = (state, paymentRulesForFee) => {
  state.paymentRulesForFee = paymentRulesForFee
}

export const DELETE_PAYMENT_RULE_FOR_FEE = (state, paymentRule) => {
  state.paymentRulesForFee = state.paymentRulesForFee.filter(function (item) {
    return paymentRule._id != item._id;
  })
}





export const GET_PAYMENT_RULE_FOR_TUITION = (state, paymentRule) => {
  state.paymentRule = paymentRule
}

export const UPDATE_PAYMENT_RULE_FOR_TUITION = (state, paymentRule) => {
  state.paymentRule = paymentRule
}

export const GET_PAYMENT_RULES_FOR_TUITION = (state, paymentRulesForTuition) => {
  state.paymentRulesForTuition = paymentRulesForTuition
}

export const DELETE_PAYMENT_RULE_FOR_TUITION = (state, paymentRule) => {
  state.paymentRulesForTuition = state.paymentRulesForTuition.filter(function (item) {
    return paymentRule._id != item._id;
  })
}



// Examination Tables

export const GET_EXAMINATION_TABLES = (state, examinationTables) => {
  state.examinationTables = examinationTables
}

export const UPDATE_EXAMINATION_TABLE = (state, examinationTable) => {
  state.examinationTable = examinationTable
}

export const DELETE_EXAMINATION_TABLE = (state, examinationTable) => {
  state.examinationTables = state.examinationTables.filter(function (item) {
    return examinationTable._id != item._id;
  })
}




// Registration to Examination Tables

export const GET_REGISTRATION_TO_EXAMINATION_TABLES = (state, registrationToExaminationTables) => {
  state.registrationToExaminationTables = registrationToExaminationTables
}

export const GET_LIST_OF_REGISTRATION_TO_EXAMINATION_TABLES = (state, listOfRegistrationToExaminationTables) => {
  state.listOfRegistrationToExaminationTables = listOfRegistrationToExaminationTables
}






// Stock of ambos

export const GET_STOCK_OF_AMBOS = (state, items) => {
  state.itemListOfStockOfAmbos = items
}

export const DELETE_STOCK_OF_AMBOS = (state, itemOfStock) => {
  state.itemListOfStockOfAmbos.docs = state.itemListOfStockOfAmbos.docs.filter(function (item) {
    var result = itemOfStock._id != item._id;
    if(!result) {
      state.itemListOfStockOfAmbos.total--;
    }
    return result;
  })
}

export const UPDATE_STOCK_OF_AMBOS = (state, item) => {
  state.itemOfStockOfAmbos = item
}




// Delivery of ambos

export const GET_DELIVERY_OF_AMBOS = (state, items) => {
  state.itemListOfDeliveryOfAmbos = items
}

export const DELETE_DELIVERY_OF_AMBOS = (state, itemOfDelivery) => {
  state.itemListOfDeliveryOfAmbos.docs = state.itemListOfDeliveryOfAmbos.docs.filter(function (item) {
    var result = itemOfDelivery._id != item._id;
    if(!result) {
      state.itemListOfDeliveryOfAmbos.total--;
    }
    return result;
  })
}

export const UPDATE_DELIVERY_OF_AMBOS = (state, item) => {
  state.itemOfDeliveryOfAmbos = item
}


// Student file lists

export const GET_STUDENT_FILE_LIST = (state, items) => {
  state.studentFileList = items
}


export const GET_STUDENT_LIST_OF_CURRENT_ACADEMIC_YEAR = (state, items) => {
  state.studentsListOfCurrentAcademicYear = items
}
