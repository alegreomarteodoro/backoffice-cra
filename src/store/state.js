export const STORAGE_KEY = 'backoffice-cra'

let syncedData = {
  auth: {
    isLoggedIn: false,
    accessToken: null,
    user: {
      email: null,
      admin: false,
      role: null,
      _id: null
    }
  },

  users: {
    docs: []
  },
  user: {
    _id: null,
    email: null,
    admin: false,
    active: false,
    role: null,
    password: null,
    displayName: null
  },

  roles: [],
  role: {
    _id: null,
    name: '',
    active: false,
    isDefault: false
  },

  permissions: [],
  permission: {
    _id: null,
    name: '',
    key: '',
    module: ''
  },

  careers: [],
  career: {},

  mobileMenuIsActive: false,

  configurations: [],

  registrationToCareer: null,

  registrationForm: {
    _id:null,
    currentCareer:null,
    currentLevel: null,
    personalData:{
      lastName:null,
      firstName:null,
      dni:null,
      gender:null,
      cuil:null,
      size:null,
      birthDate:null,
      email:null,
      birthPlace:{
        locality:null,
        province:null,
        nationality:null
      },
      currentResidence:{
        street:null,
        streetNumber:null,
        floor:null,
        departamentNumber:null,
        neighborhood:null,
        locality:null,
        province:null,
        country: null,
        zipCode:null,
        phone:null,
        cellPhone:null,
        cellPhoneCompany:null
      }
    },
    treasury: {
      paymentId: null,
      paymentMethod: null
    }
  },

  registrationToCareerList: [],

  payments: {
    docs: []
  },
  payment: {},

  paymentRule: {},
  paymentRulesForFee: [],
  paymentRulesForTuition: [],

  examinationTables: [],
  examinationTable: {},

  registrationToExaminationTables: {},
  listOfRegistrationToExaminationTables: {},

  itemListOfDeliveryOfAmbos: [],
  itemOfDeliveryOfAmbos: {},

  itemListOfStockOfAmbos: [],
  itemOfStockOfAmbos: {},

  studentFileList: [],

  studentsListOfCurrentAcademicYear: []
};

const notSyncedData = {};

// Sync with local storage.
if (localStorage.getItem(STORAGE_KEY)) {
  let localStorageData = JSON.parse(localStorage.getItem(STORAGE_KEY));
  syncedData.auth = localStorageData.auth;
}

// Merge data and export it.
export const state = Object.assign(syncedData, notSyncedData)
